from example_fastapi_app.__main__ import rest_api
from fastapi.testclient import TestClient
import pytest

client = TestClient(rest_api)


@pytest.fixture
def example_person_json():
    return {"name": "mark", "age": 25, "description": "a bad person"}


def test_post_person(example_person_json):
    response = client.post(
        "/person",
        json=example_person_json,
    )
    assert response.status_code == 200


def test_get_person(example_person_json):
    test_post_person(example_person_json)

    response = client.get(
        "/person/mark",
    )
    assert response.status_code == 200
    assert response.json() == {
        "name": "mark",
        "age": 25,
        "description": "a bad person",
    }


def test_delete_person(example_person_json):
    test_post_person(example_person_json)

    response = client.delete(
        "/person/mark",
    )
    assert response.status_code == 200


def test_put_person(example_person_json):
    test_post_person(example_person_json)

    response = client.put(
        "person/mark",
        json={"name": "mark", "age": 29, "description": "a good person"}
    )
    assert response.status_code == 200

    res = client.get(
        "/person/mark",
    )
    assert res.status_code == 200
    assert res.json() == {
        "name": "mark",
        "age": 29,
        "description": "a good person",
    }
