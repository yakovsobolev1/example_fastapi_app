from example_fastapi_app.__main__ import person
import pytest


@pytest.fixture
def example_person():
    return person("bill", 30, "a good person")


def test_person_init(example_person):
    assert example_person.name == "bill"
    assert example_person.age == 30
    assert example_person.description == "a good person"


def test_person_eq_to_another(example_person):
    example_person2 = person("bill", 30, "a good person")
    assert example_person == example_person2
