FROM python:3.8-alpine
COPY dist app/
WORKDIR /app
RUN pip install ./example_fastapi_app-0.0.1.tar.gz
ENTRYPOINT ["python","-m", "example_fastapi_app"]
CMD ["--server-ip", "0.0.0.0", "--server-port", "8080"]
EXPOSE 8080
